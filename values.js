// function values(obj) {
//     // Return all of the values of the object's own properties.
//     // Ignore functions
//     // http://underscorejs.org/#values
// }
const values = (obj) => {
    const value = [];
    for (let k in obj) {
        value.push(obj[k]);
    }
    return value;
}
module.exports = values;