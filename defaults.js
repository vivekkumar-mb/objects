const defaults = (obj, defaultvalue) => {
    if (!defaultvalue) return obj;
    for (let k in defaultvalue) {
        if (!obj.hasOwnProperty(k)) {
            obj[k] = defaultvalue[k];
            break;
        }
    }
    return obj;
}
module.exports = defaults;