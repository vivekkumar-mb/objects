const invert = require("../invert");




const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const ans=invert(testObject);
console.log(ans);//{ '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' }