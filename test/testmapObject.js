const mapObject = require("../mapObject");

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

const ans = mapObject(testObject, (key) => {

    return testObject[key] + 10;//{ name: 'Bruce Wayne10', age: 46, location: 'Gotham10' }
});

console.log(ans);