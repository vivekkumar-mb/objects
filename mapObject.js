const mapObject = (obj, cb) => {
    const newobj = {}
    for (let k in obj) {

        newobj[k] = cb(k);

    }
    return newobj;
}



module.exports = mapObject;